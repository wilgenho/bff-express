
# BFF con Express y http-proxy-middleware
Esta aplicación es una POC para probar la creación de un BFF con Express y una libreria de proxy

La libreria http-proxy-middleware permite configurar rápidamente, mediante configuración, un aplicación que funciene como BFF (Backend for Frontend). Entre otras cosas permite:
- Configurar enrutamiento
- Convertir respuestas
- Interceptar errores

## Instalar y ejecutar

Para instalar la aplicación

```
npm install
```

Ejecutar la aplicación con el siguiente comando:

```
npm run serve
```

## Crear imagen y executar imagen de Docker

Para crear la imagen:

```
docker build . -t bff-express
```

Para ejecutar

```
docker run -p 3000:3000 -d bff-express
```

## Servicios de prueba

Ejemplos:

- http://localhost:3000/posts/1
- http://localhost:3000/usuarios/1
- http://localhost:3000/provincias/2

## Referencias

- https://github.com/chimurai/http-proxy-middleware

