import {createProxyMiddleware, Options, responseInterceptor} from 'http-proxy-middleware';
import {IncomingMessage, ServerResponse} from 'http';

interface Usuario {
    id: number;
    nombre: string;
    usuario: string;
    email: string;
    telefono: string;
    web: string;
}

const convertirUsuario = (datos: Buffer): Usuario => {
    const json = JSON.parse(datos.toString('utf-8'))
    return {
        id: json.id,
        nombre: json.name,
        usuario: json.username,
        email: json.email,
        telefono: json.phone,
        web: json.website
    }
}

const usuarioInterceptor = async (buffer: Buffer, proxyRes: IncomingMessage, req: IncomingMessage, res: ServerResponse) => {
    const usuario = convertirUsuario(buffer);
    return JSON.stringify(usuario);
}

const opciones: Options = {
    target: 'http://jsonplaceholder.typicode.com',
    changeOrigin: true,
    logLevel: 'debug',
    pathRewrite: {
        '^/usuarios': '/users'
    },
    selfHandleResponse: true,
    onProxyRes: responseInterceptor(usuarioInterceptor)
};

export const userProxy = createProxyMiddleware('/usuarios/*', opciones);
