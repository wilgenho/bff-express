import {createProxyMiddleware, Options} from 'http-proxy-middleware';
import {Request} from 'express'


/**
 * 
 * Convierte una ruta con parametros en la url por ejemplo /provincia/1
 * en una ruta con una query por ejemplo /provincia?id=1
 * 
 * @param path Ruta del servicio
 * @param req Request
 * @returns Ruta convertida
 */
const convertirPath = (path: string, req: Request): string => {
    console.log(path);
    const tokens = path.split('/');
    if (tokens.length > 2) {
        return `${tokens[1]}?id=${tokens[2]}`
    }
    return path;
}

const opciones: Options = {
    target: 'https://apis.datos.gob.ar/georef/api',
    changeOrigin: true,
    logLevel: 'debug',
    pathRewrite: convertirPath
};

export const provinciaProxy = createProxyMiddleware('/provincias', opciones);
