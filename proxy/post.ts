import {createProxyMiddleware, Filter, Options, RequestHandler} from 'http-proxy-middleware';

const opciones: Options = {
    target: 'http://jsonplaceholder.typicode.com',
    changeOrigin: true,
    logLevel: 'debug'
};

export const postProxy = createProxyMiddleware('/posts', opciones);
