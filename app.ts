import express, { Request, Response, NextFunction } from 'express';
import {postProxy} from './proxy/post';
import {userProxy} from './proxy/users';
import {provinciaProxy} from './proxy/provincias';


const app = express();
const port = 3000;

app.use('/', postProxy);
app.use('/', userProxy);
app.use('/', provinciaProxy);

app.listen(port, () => {
    console.log(`Server is running on port ${port}.`)
})

const getVersion = (request: Request, response: Response, next: NextFunction) => {
    response.status(200).send('version 1.0')
};
  
app.get('/version', getVersion);